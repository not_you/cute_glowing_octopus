
#define timer r16
#define transmit r17
#define byte_left r18

#define Ylo r28
#define Yhi r29
#define Zlo r30
#define Zhi r31
#define ledbuffer_end 0x4c

#define green r19
#define red r20
#define blue r21
#define mode r22



#define CLKPS 0x36

	.org 0
init:
	LDI R29,0xD8;
	out CCP,R29;
	LDI R29,0
	out CLKPS,R29;
	;set PB2 and PB1 to output, clear output on PB1
	SBI DDRB,2;for bitbanging to LEDS
	SBI DDRB,1;for jumper
	CBI PORTB,1;
	;set PB0 to input and enable pullup
	CBI DDRB,0;
	SBI PUEB,0;
	;set Y pointer to lowest SRAM byte
	LDI Ylo,0x40;
	LDI Yhi,0x00;


	;LED1
	LDI green,0x00;
	LDI red,0xFF;
	LDI blue,0x00;


	SBIS PINB,0;
	rjmp write_buffer
	LDI mode,0;

	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;

	;LED2
	LDI green,0x00;
	LDI red,0x55;
	LDI blue,0xAA;

	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;

	;LED3
	LDI green,0x80;
	LDI red,0x00;
	LDI blue,0x7F;

	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;

	;LED4
	LDI green,0xAA;
	LDI red,0x55;
	LDI blue,0x00;

	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;

	LDI Ylo,0x40;
	LDI Yhi,0x00;


	LDI Zlo,0x60
	LDI Zhi,0x00
	ijmp;



write_buffer:
	LDI mode,1;
	;write this color 4 times to ram
	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;

	CPI Ylo,ledbuffer_end;
	BREQ write_leds;
	RJMP write_buffer;

	.org 0x33
write_leds:
	;initialize variables for sending data to leds
	LDI Ylo,0x40;
	LD transmit,Y+;
	LDI byte_left,8;
	;set LED pin high
	SBI PORTB,2;
	SBRC transmit,7;
	RJMP write_high;
write_low:
	CBI PORTB,2;
	;shift to next bit to send and count down the bits left
	LSL transmit;

	SUBI byte_left,1;
	;if current byte is done sending after this, fetch the next one
	BREQ load_byte_low;
	NOP;
	;set LED pin high and go back to the start of the send routine
	SBRC transmit,7;
	RJMP write_high;
	SBI PORTB,2;
	RJMP write_low;

load_byte_low:
	;sleep if transmit is done
	CPI Ylo,ledbuffer_end;
	BREQ main;
	;else load the next byte out of ram
	LD transmit,Y+;
	LDI byte_left,8;
	;set LED pin high and go back to the start of the send routine
	SBRC transmit,7;
	RJMP write_high;
	SBI PORTB,2;
	RJMP write_low;


write_high:
	SBI PORTB,2;
	;shift to next bit to send and count down the bits left
	LSL transmit;
	SUBI byte_left,1;
	BREQ load_byte_high;
	NOP;
	NOP;
	NOP;
	;set LED pin low
	CBI PORTB,2;
	;set LED pin high and go back to the start of the send routine
	;check what bit to send
	SBRC transmit,7;
	RJMP write_high;
	SBI PORTB,2;
	RJMP write_low;

load_byte_high:

	;sleep if transmit is done
	CPI Ylo,ledbuffer_end
	;else load the next byte out of ram
	BREQ main;

	LD transmit,Y+;
	;set LED pin low
	CBI PORTB,2;
	LDI byte_left,8;
	;set LED pin high and go back to the start of the send routine
	SBRC transmit,7;
	RJMP write_high;
	SBI PORTB,2;
	RJMP write_low;

	.org 0x60
main:


	CBI PORTB,2;
	;set clock as low as possible and whait 33ms
	LDI R29,0xD8;
	out CCP,R29;
	LDI R29,8
	out CLKPS,R29;
	LDI timer,100;
	SBRC mode,0;
	LDI timer,255
stopwatch:
	SUBI timer,1;
	NOP;
	NOP;
	NOP;
	BRNE stopwatch;
	;set clock back to 8mhz
	LDI R29,0xD8;
	out CCP,R29;
	LDI R29,0
	out CLKPS,R29;
	;make next colors
	LDI Ylo,0x40;
	LDI Yhi,0x00;

oneLED:
	CPI Ylo,ledbuffer_end
	BRSH check_mode;

	LD green,Y+;
	LD red,Y+;
	LD blue,Y+;

	CPI blue,0;
	BRNE rainbow2;
	CPI green,0;
	BRNE rainbow;
	CPI red,0;
	BRNE rainbow1;
rainbow:
	CPI blue,0;
	BRNE rainbow;

	SUBI green,1;
	INC red;
	RJMP rainbow_end;
rainbow1:
	CPI green,0;
	BRNE rainbow;
	SUBI red,1;
	INC blue;
	RJMP rainbow_end;
rainbow2:
	CPI red,0;
	BRNE rainbow1;
	SUBI blue,1;
	INC green;
	RJMP rainbow_end;
rainbow_end:
	SUBI Ylo,3;

	ST Y+,green;
	ST Y+,red;
	ST Y+,blue;
	RJMP oneLED;

check_mode:
	LDI Zhi,0x00;
	LDI Zlo,0x00;
	CPI mode,0;
	BREQ low_check_mode;
	SBIC PINB,0;
	IJMP;
	LDI Zlo,0x33;
	IJMP;
	
low_check_mode:
	SBIS PINB,0;
	IJMP;
	LDI Zlo,0x33;
	IJMP;